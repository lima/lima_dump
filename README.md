usage:

check kernel driver max error task capacity:
```
cat /sys/module/lima/parameters/max_error_tasks
echo 1 | sudo tee /sys/module/lima/parameters/max_error_tasks
```

clear previous saved error task state:
```
echo 0 | sudo tee /sys/class/drm/renderD128/device/error
```

when a task fail to execute, kernel driver will save its state and export to user by "/sys/class/drm/renderD128/device/error", user can use this tool to disassemble the error state by:
```
lima_dump dsm /sys/class/drm/renderD128/device/error
```
output result is stored at "task.dump.*".

you may change the dump file and assemble it again by:
```
lima_dump asm task.dump.0
```
output result is stored at "task.bin"

you may also replay the dump by:
```
lima_dump replay task.bin
```
