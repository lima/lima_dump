#!makefile

CFLAGS += -std=gnu99 -g $(shell pkg-config --cflags libdrm)
LDFLAGS += $(shell pkg-config --libs libdrm)

lima_dump: main.c lima_dump.h
	gcc $(CFLAGS) -g -o $@ $^ $(LDFLAGS)
