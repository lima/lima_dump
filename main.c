#include <stdio.h>
#include <assert.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include <sys/mman.h>

#include <xf86drm.h>

#include "lima_dump.h"
#include "lima_drm.h"

#define DUMP_FILE_NAME "task.dump"
#define BIN_FILE_NAME "task.bin"

static char *task_names[LIMA_DUMP_TASK_NUM] = {
	[LIMA_DUMP_TASK_GP] = "GP",
	[LIMA_DUMP_TASK_PP] = "PP",
};

static char *chunk_names[LIMA_DUMP_CHUNK_NUM] = {
	[LIMA_DUMP_CHUNK_FRAME]        = "FRAME",
	[LIMA_DUMP_CHUNK_BUFFER]       = "BUFFER",
	[LIMA_DUMP_CHUNK_PROCESS_NAME] = "PNAME",
	[LIMA_DUMP_CHUNK_PROCESS_ID]   = "PID",
};

static void dump_blob(FILE *fp, void *data, int size)
{
	for (int i = 0; i * 4 < size; i += 4) {
		bool skip = true;

		/* if all data zero, skip this line */
		for (int j = 0; j < 4 && (i + j) * 4 < size; j++) {
			if (((uint32_t *)data)[i + j]) {
				skip = false;
				break;
			}
		}

		if (skip)
			continue;

		fprintf(fp, "%08x:", i * 4);
		for (int j = 0; j < 4 && (i + j) * 4 < size; j++)
			fprintf(fp, " %08x", ((uint32_t *)data)[i + j]);
		fprintf(fp, "\n");
	}
}

static int open_dump_file(const char *filename, struct lima_dump_head *head)
{
	int fd = open(filename, O_RDONLY);
	assert(fd >= 0);

	assert(read(fd, head, sizeof(*head)) == sizeof(*head));

	assert(head->magic == LIMA_DUMP_MAGIC);
	assert(head->version_major == LIMA_DUMP_MAJOR);

	if (head->version_minor > LIMA_DUMP_MINOR)
		printf("warning: dump file version is newer than this tool\n");

	printf("total size %d num_tasks %d\n", head->size, head->num_tasks);

	return fd;
}

static void dump_dsm(const char *filename)
{
	struct lima_dump_head head;
	int fd = open_dump_file(filename, &head);

	char name[PATH_MAX];
	for (int i = 0; i < head.num_tasks; i++) {
		snprintf(name, sizeof(name), DUMP_FILE_NAME ".%d", i);
		FILE *of = fopen(name, "w+");
		assert(of);

		struct lima_dump_task task;
		assert(read(fd, &task, sizeof(task)) == sizeof(task));

		fprintf(of, "TASK %s SIZE %08x CHUNKS %d\n\n",
			task.id < LIMA_DUMP_TASK_NUM ? task_names[task.id] :
			"UNKNOWN", task.size, task.num_chunks);

		for (int j = 0; j < task.num_chunks; j++) {
			struct lima_dump_chunk chunk;
			assert(read(fd, &chunk, sizeof(chunk)) == sizeof(chunk));

			fprintf(of, "CHUNK %s SIZE %08x",
				chunk.id < LIMA_DUMP_CHUNK_NUM ? chunk_names[chunk.id] :
				"UNKNOW", chunk.size);

			switch (chunk.id) {
			case LIMA_DUMP_CHUNK_BUFFER: {
				struct lima_dump_chunk_buffer *cb =
					(struct lima_dump_chunk_buffer *)&chunk;
				fprintf(of, " VA %08x\n", cb->va);
				break;
			}
			case LIMA_DUMP_CHUNK_PROCESS_ID: {
				struct lima_dump_chunk_pid *cp =
					(struct lima_dump_chunk_pid *)&chunk;
				fprintf(of, " %d\n", cp->pid);
				break;
			}
			default:
				fprintf(of, "\n");
				break;
			}

			void *data = malloc(chunk.size);
			assert(data);

			int size = 0;
			while (size < chunk.size) {
				int ret = read(fd, data + size, chunk.size - size);
				assert(ret > 0);
				size += ret;
			}

			switch (chunk.id) {
			case LIMA_DUMP_CHUNK_PROCESS_NAME:
				fprintf(of, "%s\n", (char *)data);
				break;
			case LIMA_DUMP_CHUNK_PROCESS_ID:
				break;
			default:
				dump_blob(of, data, chunk.size);
				break;
			}

			fprintf(of, "\n");
		}

		fclose(of);
	}

	close(fd);
}

static void scan_blob(FILE *f, void *data)
{
	int ret;
	size_t n = 128;
	char *line = malloc(n);

	while ((ret = getline(&line, &n, f)) != -1) {
		if (ret == 1 && line[0] == '\n')
			return;

		int off;
		uint32_t value[4];
		int num = (ret - 10) / 9;
		switch (num) {
		case 1:
			assert(sscanf(line, "%x: %x\n",
				      &off, value) == 2);
			break;
		case 2:
			assert(sscanf(line, "%x: %x %x\n",
				      &off, value, value + 1) == 3);
			break;
		case 3:
			assert(sscanf(line, "%x: %x %x %x\n",
				      &off, value, value + 1, value + 2) == 4);
			break;
		case 4:
			assert(sscanf(line, "%x: %x %x %x %x\n",
				      &off, value, value + 1, value + 2, value + 3) == 5);
			break;
		default:
			assert(0);
			break;
		}

		memcpy(data + off, value, num * 4);
	}

	free(line);
}

static int write_task(int fd, const char *filename)
{
	FILE *f = fopen(filename, "r");
	assert(f);

	char name[32];
	int size, num_chunks;
	assert(fscanf(f, "TASK %s SIZE %x CHUNKS %d\n\n", name, &size, &num_chunks) == 3);

	struct lima_dump_task task = {
		.size = size,
		.num_chunks = num_chunks,
	};
	if (!strcmp("GP", name))
		task.id = LIMA_DUMP_TASK_GP;
	else if (!strcmp("PP", name))
		task.id = LIMA_DUMP_TASK_PP;
	else
		assert(0);

	assert(write(fd, &task, sizeof(task)) == sizeof(task));

	for (int i = 0; i < num_chunks; i++) {
		assert(fscanf(f, "CHUNK %s SIZE %x", name, &size) == 2);
		struct lima_dump_chunk chunk = {
			.size = size,
		};
		if (!strcmp("FRAME", name))
			chunk.id = LIMA_DUMP_CHUNK_FRAME;
		else if (!strcmp("PNAME", name))
			chunk.id = LIMA_DUMP_CHUNK_PROCESS_NAME;
		else if (!strcmp("PID", name)) {
			chunk.id = LIMA_DUMP_CHUNK_PROCESS_ID;
			struct lima_dump_chunk_pid *cp =
				(struct lima_dump_chunk_pid *)&chunk;
			assert(fscanf(f, " %d", &cp->pid) == 1);
		} else if (!strcmp("BUFFER", name)) {
			chunk.id = LIMA_DUMP_CHUNK_BUFFER;
			struct lima_dump_chunk_buffer *cb =
				(struct lima_dump_chunk_buffer *)&chunk;
			assert(fscanf(f, " VA %x", &cb->va) == 1);
		} else
			assert(0);

		assert(write(fd, &chunk, sizeof(chunk)) == sizeof(chunk));

		assert(fgetc(f) == '\n');

		char *data = calloc(size, 1);
		assert(data || !size);

	        if (chunk.id == LIMA_DUMP_CHUNK_PROCESS_NAME)
			assert(fscanf(f, "%s\n", data) == 1);
		else if (chunk.id == LIMA_DUMP_CHUNK_FRAME ||
			 chunk.id == LIMA_DUMP_CHUNK_BUFFER)
			scan_blob(f, data);

		int ws = 0;
		while (ws < size) {
			int ret = write(fd, data + ws, size - ws);
			assert(ret > 0);
			ws += ret;
		}

		free(data);

		fscanf(f, "\n");
	}

	fclose(f);
	return task.size + sizeof(task);
}

static void dump_asm(int num_file, char **filenames)
{
	int fd = open(BIN_FILE_NAME, O_RDWR | O_CREAT);
	assert(fd >= 0);

	struct lima_dump_head head = {
		.magic = LIMA_DUMP_MAGIC,
		.version_major = LIMA_DUMP_MAJOR,
		.version_minor = LIMA_DUMP_MINOR,
	};

	assert(write(fd, &head, sizeof(head)) == sizeof(head));

	for (int i = 0; i < num_file; i++) {
		int ret = write_task(fd, filenames[i]);
		if (ret <= 0) {
			printf("skip file %s\n", filenames[i]);
			continue;
		}

		head.size += ret;
		head.num_tasks++;
	}

	assert(lseek(fd, 0, SEEK_SET) == 0);
	assert(write(fd, &head, sizeof(head)) == sizeof(head));

	close(fd);
}

struct replay_drm {
	int fd;
	int ctx;
	uint32_t sync;
};

static struct replay_drm drm = {0};

static void init_drm(void)
{
	drm.fd = drmOpenWithType("lima", NULL, DRM_NODE_RENDER);
	assert(drm.fd > 0);

	drmVersionPtr version = drmGetVersion(drm.fd);
        assert(version);

	assert(version->version_major > 1 || version->version_minor > 1);

	drmFreeVersion(version);

	struct drm_lima_ctx_create ctx_req = {0};
	assert(!drmIoctl(drm.fd, DRM_IOCTL_LIMA_CTX_CREATE, &ctx_req));
	drm.ctx = ctx_req.id;

	assert(!drmSyncobjCreate(drm.fd, DRM_SYNCOBJ_CREATE_SIGNALED, &drm.sync));
}

struct lima_bo {
	uint32_t handle;
	uint32_t va;
	uint32_t size;
	uint64_t offset;
	void *map;
};

static void create_bo(struct lima_bo *bo, uint32_t size, uint32_t va)
{
	struct drm_lima_gem_create create_req = {
		.size = size,
		.flags = LIMA_BO_FLAG_FORCE_VA,
		.va = va,
	};

	assert(!drmIoctl(drm.fd, DRM_IOCTL_LIMA_GEM_CREATE, &create_req));

	bo->handle = create_req.handle;
	bo->va = va;
	bo->size = size;

	struct drm_lima_gem_info info_req = {
		.handle = bo->handle,
	};

	assert(!drmIoctl(drm.fd, DRM_IOCTL_LIMA_GEM_INFO, &info_req));

	bo->offset = info_req.offset;
	assert(bo->va == info_req.va);

	bo->map = mmap(0, bo->size, PROT_READ | PROT_WRITE,
		       MAP_SHARED, drm.fd, bo->offset);
	assert(bo->map);
}

static void free_bo(struct lima_bo *bo)
{
	munmap(bo->map, bo->size);

	struct drm_gem_close args = {
		.handle = bo->handle,
	};

	assert(!drmIoctl(drm.fd, DRM_IOCTL_GEM_CLOSE, &args));
}

#define VOID2U64(x) ((uint64_t)(unsigned long)(x))

static void replay_task(int fd, struct lima_dump_task *task)
{
	void *frame = NULL;
	int frame_size = 0;

	struct lima_bo *bos = malloc(sizeof(struct lima_bo) * task->num_chunks);
	assert(bos);
	int num_bos = 0;

	for (int i = 0; i < task->num_chunks; i++) {
		struct lima_dump_chunk chunk;
		assert(read(fd, &chunk, sizeof(chunk)) == sizeof(chunk));

		void *data;
		switch (chunk.id) {
		case LIMA_DUMP_CHUNK_FRAME:
			frame = malloc(chunk.size);
			assert(frame);
			frame_size = chunk.size;
			data = frame;
			break;
		case LIMA_DUMP_CHUNK_BUFFER: {
			struct lima_dump_chunk_buffer *cb =
				(struct lima_dump_chunk_buffer *)&chunk;
			create_bo(bos + num_bos, cb->size, cb->va);
			data = bos[num_bos].map;
			num_bos++;
			break;
		}
		default:
			lseek(fd, chunk.size, SEEK_CUR);
			continue;
		}

		int size = 0;
		while (size < chunk.size) {
			int ret = read(fd, data + size, chunk.size - size);
			assert(ret > 0);
			size += ret;
		}
	}

	assert(frame);
	assert(num_bos);

	struct drm_lima_gem_submit_bo *sbos = malloc(sizeof(*sbos) * num_bos);
	for (int i = 0; i < num_bos; i++) {
		sbos[i].handle = bos[i].handle;
		sbos[i].flags = LIMA_SUBMIT_BO_WRITE | LIMA_SUBMIT_BO_READ;
	}

	struct drm_lima_gem_submit req = {
		.ctx = drm.ctx,
		.pipe = task->id,
		.nr_bos = num_bos,
		.bos = VOID2U64(sbos),
		.frame = VOID2U64(frame),
		.frame_size = frame_size,
		.out_sync = drm.sync,
	};

	assert(!drmIoctl(drm.fd, DRM_IOCTL_LIMA_GEM_SUBMIT, &req));

	assert(!drmSyncobjWait(drm.fd, &drm.sync, 1, INT64_MAX, 0, NULL));

	free(sbos);
	free(frame);
	for (int i = 0; i < num_bos; i++)
		free_bo(bos + i);
	free(bos);
}

static void free_drm(void)
{
	drmSyncobjDestroy(drm.fd, drm.sync);

	struct drm_lima_ctx_free req = {
		.id = drm.ctx,
	};

	drmIoctl(drm.fd, DRM_IOCTL_LIMA_CTX_FREE, &req);

	close(drm.fd);
}

static void dump_replay(const char *filename)
{
	init_drm();

	struct lima_dump_head head;
	int fd = open_dump_file(filename, &head);

	for (int i = 0; i < head.num_tasks; i++) {
		struct lima_dump_task task;
		assert(read(fd, &task, sizeof(task)) == sizeof(task));

		replay_task(fd, &task);
	}

	close(fd);

	free_drm();
}

static void usage(const char *name)
{
	printf("usage: %s asm|dsm|replay ...\n", name);
	printf("       %s asm <text dump file 1> <text dump file 2> ...\n", name);
	printf("       %s dsm <bin dump file>\n", name);
	printf("       %s replay <bin dump file>\n", name);
}

int main(int argc, char **argv)
{
	if (argc < 2) {
		usage(argv[0]);
		return -1;
	}

	if (!strcmp("dsm", argv[1])) {
		if (argc != 3) {
			usage(argv[0]);
			return -1;
		}
		dump_dsm(argv[2]);
	} else if (!strcmp("replay", argv[1])) {
		if (argc != 3) {
			usage(argv[0]);
			return -1;
		}
		dump_replay(argv[2]);
	} else if (!strcmp("asm", argv[1])) {
		if (argc < 3) {
			usage(argv[0]);
			return -1;
		}
		dump_asm(argc - 2, argv + 2);
	}
	else {
		usage(argv[0]);
		return -1;
	}

	return 0;
}
